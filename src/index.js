import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import FactSheet from './components/FactSheet';

ReactDOM.render(<FactSheet />, document.getElementById('root'));
