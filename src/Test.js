import React from 'react';
import ReactDOM from 'react-dom';
import FactSheet from './FactSheet';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<FactSheet />, div);
  ReactDOM.unmountComponentAtNode(div);
});
