import React from 'react';

const AnimalFact = ( {location, animal, adjective, action, alignment} ) => {
  return (
    <div>
      <p>
      { location.place_locative_name } on liikkeellä  
      </p> 
      <p>     
      <span id="adjectiveanimaltext"><a href="/" onClick = { action } > { adjective }{ adjective.endsWith('-') ? '' : ' '}{animal}</a></span>
      { alignment==='neutral' ? '.' : '!' } 
      </p>
    </div>
  )
}
export default AnimalFact