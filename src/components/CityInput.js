import React from 'react';

const CityInput = ( {city} ) => {

  return (
  <div>
    <form action="">
      <input type="text" name="" placeholder={'Valitse sijainti - ' + city} value=""></input>
    </form>
  </div>
  )
}

export default CityInput