import React from 'react';

const Picture = ( {alignment} ) => {

  const choosePhoto = a => {
    const now = new Date();
    const time10Secs = Math.floor(now.getTime() / 10000);
    console.log(time10Secs);
    switch (a) {
      case 'mean': return ('https://source.unsplash.com/collection/3655744/692x464?sig=' + time10Secs)
      case 'cute': return ('https://source.unsplash.com/collection/3656143/692x464?sig=' + time10Secs)
      default: return './images/jay.jpg'
      }
    }
  
  return (
    <div>
      <img src={ choosePhoto(alignment) } alt="kuva" className="paakuva"></img>
    </div>
    )
};

export default Picture;