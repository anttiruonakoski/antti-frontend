const adjectives = 
{
  cute: [ "söpö", "pörröinen", "ilonen", "viehko-", "ystävällinen", "avulias", "jalo", "rohkea", "vaaleanpunainen", "suomalais-", "pyhimys-", "ihana", "kuuma", 
    "kultainen", "kaunopuheinen", "viisas", "legendaarinen"],
  neutral: [ "kirjava", "harmaa", "nopea", "liukas", "nelijalkainen", "ujo", "hiljainen", "tavallinen", "keskinkertainen", "nuori", 
    "punainen", "vapaa", "iso", "pieni", "arki-", "viimevuotinen", "muutto-", "lapin-", "uninen", "vikkelä", "paksu"],
  mean: ["ketku-", "myrkyllinen", "musta", "väkivaltainen", "tautinen", "sairas", "kyttäävä", "laiska", "ilkiö-", "karannut", "zombi-", "noiduttu", "aseistettu",
  "ulostava", "idiootti-", "kateellinen"]
};

export default adjectives