import axios from 'axios'
const baseUrl = 'http://94.237.56.155:3000'

const getLocationNames = (city) => {
  return axios.get( baseUrl + '/municipalities/' + city) 
}

export default getLocationNames