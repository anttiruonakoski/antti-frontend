import animals from './data/animals'
import adjectives from './data/adjectives'
  
  const rollAnimal = () => {
    let numero = Math.floor(Math.random()*animals.length)
    console.log('rollattu eläin', numero)
    return animals[numero]
  }
  
  const rollAdjective = () => {

    const rollAlignment = () => ['cute', 'mean', 'neutral'][Math.floor(Math.random()*3)]    
    const alignment = rollAlignment()
    let numero = Math.floor(Math.random()*adjectives[alignment].length)
    console.log('rollattu adjektiivi', numero)
    return {adjective: adjectives[alignment][numero], 
           alignment,
    }
  }  

export { rollAnimal, rollAdjective }