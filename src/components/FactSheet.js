import React, { Component } from 'react'

import Picture from './Picture'
import CityInput from './CityInput'
import AnimalFact from './AnimalFact'
import '../App.css'
import {rollAnimal, rollAdjective} from './services/AdjectiveAnimalService'
import {rollLocation} from './services/LocationRollService'
import getLocationNames from './services/LocationNameService'

class FactSheet extends Component {
  constructor (props) {
    super(props)
    this.state = {
      animal: 'kettu',
      adjective: 'ketku-',
      alignment: 'mean',
      city: 'Oulu',
      locationNames: '',
      location: '',
    }  
  }

  componentDidMount() {
    getLocationNames(this.state.city)
      .then(response => {
        this.setState({ locationNames: response.data })
        this.setState({ location: rollLocation(this.state.locationNames) })
    })
  } 

  handleAdjectiveAnimalClick = (event) => {
    event.preventDefault()
    const animal = rollAnimal()
    const rolled = rollAdjective()
    const adjective = rolled.adjective
    const alignment = rolled.alignment
    
    const location = rollLocation(this.state.locationNames)

    this.setState({ animal,
                    adjective,
                    alignment,
                    location,
    })
  }

  render() {
    const headers = [
      { 
        alignment: 'cute',
        symbol: 'fa-camera-retro',
        text: 'TIEDOTUS',
      },
      { 
        alignment: 'neutral',
        symbol: 'fa-lightbulb',
        text: 'HUOMIO',
      },
      {
        alignment: 'mean',
        symbol: 'fa-exclamation-triangle',
        text: 'VAROITUS',
      },
    ]
    
    let header = headers.filter((header) => (header.alignment === this.state.alignment) ? header : undefined)[0]
    console.log(header)

    return (
      <div className={ "container " + header.alignment } >
        <h1 className="animated fadeIn"><i className={ "fas " + header.symbol }></i> { header.text }</h1>
        <div className="polaroid animated fadeIn">
            <Picture alignment={this.state.alignment} />
            <CityInput city={this.state.city} /> 
            <AnimalFact location={this.state.location} animal={this.state.animal} adjective={this.state.adjective} action={this.handleAdjectiveAnimalClick} alignment={this.state.alignment} />
        </div>
      </div>
    );
  }
}

export default FactSheet
